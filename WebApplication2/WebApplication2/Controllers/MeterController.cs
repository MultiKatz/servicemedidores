﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Utility;

namespace WebApplication2.Controllers
{
    public class MeterController : ApiController
    {
      
        public List<Meter> Get()
        {

            List<Meter> meters = new List<Meter>();

            for (int i = 1; i < 5; i++)
            {
                meters.Add(new Meter()
                {
                    ID = i,
                    Name = "Medidor RK" + RandomNumber.get(1, 100) + "Q" + i,
                    Value = RandomNumber.get(200, 1500)
                });
            }


            return meters;
        }
    }
}
