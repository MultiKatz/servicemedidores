﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Meter
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
    }
}